import { Injectable } from '@angular/core';

@Injectable()
export class UsersService {

    users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]
  getUsers(){
		return this.users;
	}
  constructor() { }

  

}