
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

  user;
  constructor() { }

  ngOnInit() {
  }

}